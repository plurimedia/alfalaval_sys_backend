# Alfalaval backend Size your system

### Pre-setup
```
Installare nvm (gestore di versioni di node) e tramite questo software scaricare la versione di node indicata nel file .nvmrc
Installare nodemon globalmente (refresha il server ad ogni modifica, altrimenti dovresti chiudere e riavviare manualmente)
```

## Project setup
```
npm install
```

### Esecuzione in locale
```
npm run dev
```
Se dà opensslErrorStack code: 'ERR_OSSL_EVP_UNSUPPORTED'; prima di lanciare aggiungere la ENV con questo comando:
export NODE_OPTIONS=--openssl-legacy-provider

### Heroku deploy
heroku git:remote -a alfalaval-sys-backend
git push heroku master
