FROM node:12-alpine

# Define Variables
ARG ApplicationName="alfalaval"
ARG ApplicationRoot="/opt/app-root/$ApplicationName"
ARG ApplicationGroup="nodejs"

# Copy files
COPY ./src "$ApplicationRoot"/src
COPY ./server "$ApplicationRoot"/server
COPY ./public "$ApplicationRoot"/public
COPY ./vue.config.js "$ApplicationRoot"/vue.config.js
COPY ./babel.config.js "$ApplicationRoot"/babel.config.js
COPY ./server.js "$ApplicationRoot"/server.js
COPY ./package.json "$ApplicationRoot"/package.json
COPY ./ibm_mongodb_ca.pem "$ApplicationRoot"/ibm_mongodb_ca.pem

WORKDIR "$ApplicationRoot"

RUN npm install && \
  npm install --only=dev && \
  npm install -g @vue/cli && \
  npm install -g @vue/cli-service-global && \
  npm run build

CMD node server.js
