import { PutObjectCommand, GetObjectCommand, HeadObjectCommand, DeleteObjectCommand } from '@aws-sdk/client-s3'
import { getSignedUrl } from '@aws-sdk/s3-request-presigner'
import { S3Client } from '@aws-sdk/client-s3'

// process.env.AWS_REGION = 'eu-south-1'
const awsBucket = process.env.AWS_BUCKET
const awsRegion = process.env.AWS_REGION
const awsAccessKey = process.env.AWS_ACCESS_KEY_ID
const awsSecretAccessKey = process.env.AWS_SECRET_ACCESS_KEY

const s3 = new S3Client({
  credentials: {
    accessKeyId: awsAccessKey,
    secretAccessKey: awsSecretAccessKey
  },
  signatureVersion: 'v4',
  region: awsRegion, /*// process.env.AWS_REGION/* ,
  useAccelerateEndpoint: true */
})

export const signedUrl = async (path, tipo) => {
  /* console.log(path, tipo)
  console.log('"' + process.env.AWS_ACCESS_KEY_ID + '"' + '"' + process.env.AWS_SECRET_KEY + '"', process.env.AWS_BUCKET) */
  const s3Params = {
    // Bucket: 'siena-imaging-storage-bucket',
    Bucket: awsBucket,
    Key: path,
    ContentType: tipo
  }

  const command = new PutObjectCommand(s3Params)

  const opts = {
    expiresIn: 60
  }

  const signed = await getSignedUrl(s3, command, opts)
  // console.log(signed)
  return signed
}

export const signedUrlDownload = async (path) => {
  // console.log('"' + process.env.AWS_ACCESS_KEY_ID + '"' + '"' + process.env.AWS_SECRET_KEY + '"', process.env.AWS_BUCKET)
  const s3Params = {
    // Bucket: 'siena-imaging-storage-bucket',
    Bucket: awsBucket,
    Key: path
  }

  const command = new GetObjectCommand(s3Params)

  const signed = await getSignedUrl(s3, command, { expiresIn: 60 })
  // console.log(signed)
  return signed
}

export const deleteFile = async (path) => {
  const s3Params = {
    Bucket: awsBucket,
    Key: path
  }

  const command = new DeleteObjectCommand(s3Params)

  const response = await s3.send(command)

  return response
}

// https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/client/s3/command/HeadObjectCommand/
export const headFile = async (path, hash) => {
  const s3Params = {
    Bucket: awsBucket,
    Key: path/* ,
    ChecksumMode: "ENABLED",
    ContentMD5: hash */
  }

  const command = new HeadObjectCommand(s3Params)

  const response = await s3.send(command)

  return response  
}
