const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const modelloSchema = new Schema({
  importanza: {
    type: String,
  },
  slug: {
    type: String,
  },
  name: {
    type: String,
    required: true,
  },
  categoria: mongoose.ObjectId,
  subtitle: {
    type: String,
  },
  cover: {
    type: Schema.Types.Mixed,
  },
  imgproducts: {
    type: Schema.Types.Mixed,
  },
  abstract: {
    type: String,
  },
  sizes: {
    type: Schema.Types.Mixed,
  },
  configurations: {
    type: String,
  },
  moreinfo_column1: {
    type: String,
  },
  moreinfo_column2: {
    type: String,
  },
  mdate: { type: Date, default: Date.now },
  documents: {
    type: Array,
    default: [],
  },
});

modelloSchema.index({ name: 1 });

mongoose.model("Modello", modelloSchema, "Modello");
