const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const datausageSchema = new Schema({
  productId: {
    type: mongoose.ObjectId,
    required: true,
  },
  product: {
    type: String,
  },
  category: {
    type: String,
  },
  mdate: {
    type: Date,
    default: Date.now,
  },
});

mongoose.model("Datausage", datausageSchema, "Datausage");
