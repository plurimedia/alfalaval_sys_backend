const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const prodottoSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  modello: {
    type: mongoose.ObjectId,
  },
  size: {
    type: String,
  },
  cover: {
    type: Schema.Types.Mixed,
  },
  imgproducts: {
    type: Schema.Types.Mixed,
  },
  abstract: {
    type: String,
  },
  mdate: {
    type: Date,
    default: Date.now,
  },
  documents: {
    type: Array,
    default: [],
  },
  other: {
    type: Schema.Types.Mixed,
  },
});

prodottoSchema.index({ name: 1 });

mongoose.model("Prodotto", prodottoSchema, "Prodotto");
