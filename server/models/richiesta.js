const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const richiestaSchema = new Schema({
  file: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
  categoryId: {
    type: mongoose.ObjectId,
    required: true,
  },
  elementId: {
    type: mongoose.ObjectId,
    required: true,
  },
  typename: {
    type: String,
    required: true,
  },
  username: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  company: {
    type: String,
  },
  phone: {
    type: String,
  },
  mdate: {
    type: Date,
    default: Date.now,
  },
});

richiestaSchema.index({ file: 1, name: 1, email: 1 });

mongoose.model("Richiesta", richiestaSchema, "Richiesta");
