const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const categoriaSchema = new Schema({
  importanza: {
    type: String,
  },
  shortname: {
    type: String,
  },
  name: {
    type: String,
    required: true,
  },
  subtitle: {
    type: String,
  },
  cover: {
    type: Schema.Types.Mixed,
  },
  imgproducts: {
    type: Schema.Types.Mixed,
  },
  abstract: {
    type: String,
  },
  moreinfo_column1: {
    type: String,
  },
  moreinfo_column2: {
    type: String,
  },
  mdate: {
    type: Date,
    default: Date.now,
  },
  documents: {
    type: Array,
    default: [],
  },
  slug: {
    type: String,
  },
});

categoriaSchema.index({ name: 1 });

mongoose.model("Categoria", categoriaSchema, "Categoria");
