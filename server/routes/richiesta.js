const mongoose = require("mongoose");
const Richiesta = mongoose.model("Richiesta");

//CRUD Richiesta
exports.getAllRichieste = (req, res) => {
  console.info("Estrazione richieste in corso");
  Richiesta.find()
    .then((results) => {
      res.status(200).send(results);
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.getRichiestaById = (req, res) => {
  console.info("Estrazione richiesta in corso");
  let id = req.query.id;
  Richiesta.find({ _id: id })
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.getRichiestaByUser = (req, res) => {
  console.info("Estrazione richiesta in corso");
  let user = req.query.user;
  Richiesta.find({ user: user })
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.saveRichiesta = (req, res) => {
  let newRichiesta = new Richiesta(req.body);
  console.info("Salvataggio richiesta in corso:", newRichiesta);

  newRichiesta
    .save()
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => {
      console.log("Err on save richiesta", err);
      res.status(500).send("Errore CRUD db")});
};

exports.updateRichiesta = (req, res) => {
  let newRichiesta = new Richiesta(req.body);
  console.info("Aggiornamento richiesta in corso:", newRichiesta);

  let id = req.body._id;
  Richiesta.findByIdAndUpdate(id, newRichiesta)
    .then((result) => {
      res.end();
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.eliminaRichiesta = (req, res) => {
  let RichiestaToDelete = req.body._id;
  console.info("Eliminazione richiesta in corso:", RichiestaToDelete);

  Richiesta.deleteOne({ _id: RichiestaToDelete })
    .then((result) => {
      if (result.deletedCount === 0) {
        res.send("La richiesta da eliminare non è presente");
      } else {
        res.end();
      }
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.getNumRichieste = (req, res) => {
  Richiesta.countDocuments()
    .then((result) => {
      res.status(200).send((result).toString());
    })
    .catch((err) => console.log("Errore conteggio Richieste", err));
};

exports.getNumUsers = (req, res) => {
  Richiesta.distinct("email")
    .then((result) => {
      res.status(200).send((result.length).toString());
    })
    .catch((err) => console.log("Errore conteggio Richieste", err));
};

exports.getRichiesteProdotti = (req, res) => {
  Richiesta.find({ type : "prodotto"}).countDocuments()
    .then((result) => {
      res.status(200).send((result).toString());
    })
    .catch((err) => console.log("Errore conteggio Richieste", err));
};