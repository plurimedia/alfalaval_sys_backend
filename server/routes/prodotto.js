const mongoose = require("mongoose");
const Prodotto = mongoose.model("Prodotto");

//CRUD Prodotto
exports.getAllProdotti = (req, res) => {
  console.info("Estrazione prodotti in corso");
  Prodotto.find()
    .then((results) => {
      res.status(200).send(results);
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.getProdottoById = (req, res) => {
  console.info("Estrazione prodotto in corso");
  let id = req.query.id;
  Prodotto.find({ _id: id })
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.getProdottoByName = (req, res) => {
  console.info("Estrazione prodotto in corso");
  let name = req.query.name;
  Prodotto.find({ name: name })
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.saveProdotto = (req, res) => {
  let newProdotto = new Prodotto(req.body);
  console.info("Salvataggio prodotto in corso:", newProdotto);

  newProdotto
    .save()
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.updateProdotto = (req, res) => {
  let newProdotto = new Prodotto(req.body);
  console.info("Aggiornamento prodotto in corso:", newProdotto);

  let id = req.body._id;
  Prodotto.findByIdAndUpdate(id, newProdotto)
    .then((result) => {
      res.end();
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.updateDocProdotto = (req, res) => {
  //recuperare i documents
  let id = req.query.id;
  let row = req.body; //c'è il singolo document
  row.mdate = new Date(); //aggiorno data modifica

  Prodotto.find({ _id: id })
    .lean()
    .then((result) => {
      //updatare il doc corrispondente
      let newProdotto = result[0];
      let toUpdate = newProdotto.documents.map((el) => {
        if (el.name === row.name && el.file === row.file) el = row;
        return el;
      });

      newProdotto.documents = toUpdate;
      console.info("Aggiornamento prodotto in corso:", newProdotto);

      Prodotto.findByIdAndUpdate(id, newProdotto)
        .then((result) => {
          res.end();
        })
        .catch((err) => res.status(500).send("Errore CRUD db"));
    })
    .catch((err) => {
      console.log("Error prodotto Update", err);
      res.status(500).send("Errore CRUD db");
    });
}

exports.eliminaProdotto = (req, res) => {
  let ProdottoToDelete = req.body._id;
  console.info("Eliminazione prodotto in corso:", ProdottoToDelete);

  Prodotto.deleteOne({ _id: ProdottoToDelete })
    .then((result) => {
      if (result.deletedCount === 0) {
        res.send("Il prodotto da eliminare non è presente");
      } else {
        res.end();
      }
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.getNumProdotti = (req, res) => {
  Prodotto.countDocuments()
    .then((result) => {
      res.status(200).send((result).toString());
    })
    .catch((err) => console.log("Errore conteggio Prodotti", err));
};
