const mongoose = require("mongoose");
const Categoria = mongoose.model("Categoria");

//CRUD Categoria
exports.getAllCategorie = (req, res) => {
  console.info("Estrazione categorie in corso");
  Categoria.find()
  .sort("importanza")
  .collation({locale: "en_US", numericOrdering: true})
    .then((results) => {
      res.status(200).send(results);
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.getCategoriaById = (req, res) => {
  console.info("Estrazione categoria in corso");
  let id = req.query.id;
  Categoria.find({ _id: id })
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.getCategoriaByName = (req, res) => {
  console.info("Estrazione categoria in corso");
  let name = req.query.name;
  Categoria.find({ name: name }).sort(parseInt("importanza"))
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.saveCategoria = (req, res) => {
  let newCategoria = new Categoria(req.body);
  console.info("Salvataggio categoria in corso:", newCategoria);

  newCategoria
    .save()
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.updateCategoria = (req, res) => {
  let newCategoria = new Categoria(req.body);
  console.info("Aggiornamento categoria in corso:", newCategoria);

  let id = req.body._id;
  Categoria.findByIdAndUpdate(id, newCategoria)
    .then((result) => {
      res.end();
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.updateDocCategoria = (req, res) => {
  //recuperare i documents
  let id = req.query.id;
  let row = req.body; //c'è il singolo document
  row.mdate = new Date(); //aggiorno data modifica

  Categoria.find({ _id: id })
    .lean()
    .then((result) => {
      //updatare il doc corrispondente
      let newCategoria = result[0];
      let toUpdate = newCategoria.documents.map((el) => {
        if (el.name === row.name && el.file === row.file) el = row;
        return el;
      });

      newCategoria.documents = toUpdate;
      console.info("Aggiornamento categoria in corso:", newCategoria);

      Categoria.findByIdAndUpdate(id, newCategoria)
        .then((result) => {
          res.end();
        })
        .catch((err) => res.status(500).send("Errore CRUD db"));
    })
    .catch((err) => {
      console.log("Error categoria Update", err);
      res.status(500).send("Errore CRUD db");
    });
}

exports.eliminaCategoria = (req, res) => {
  let categoriaToDelete = req.body._id;
  console.info("Eliminazione categoria in corso:", categoriaToDelete);

  Categoria.deleteOne({ _id: categoriaToDelete })
    .then((result) => {
      if (result.deletedCount === 0) {
        res.send("La categoria da eliminare non è presente");
      } else {
        res.end();
      }
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.getNumCategorie = (req, res) => {
  Categoria.countDocuments()
    .then((result) => {
      res.status(200).send((result).toString());
    })
    .catch((err) => console.log("Errore conteggio Categorie", err));
};
