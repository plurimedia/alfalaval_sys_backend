const mongoose = require("mongoose");
const Datausage = mongoose.model("Datausage");

//CRUD Datausage
exports.getAllDataUsage = (req, res) => {
  console.info("Estrazione contatori in corso");
  Datausage.find()
    .then((results) => {
      res.status(200).send(results);
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.getDataUsageById = (req, res) => {
  console.info("Estrazione contatore in corso");
  let id = req.query.id;
  Datausage.find({ _id: id })
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.saveDataUsage = (req, res) => {
  let newDataUsage = new Datausage(req.body);
  console.info("Salvataggio contatore in corso:", newDataUsage);

  newDataUsage
    .save()
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.updateDataUsage = (req, res) => {
  let newDataUsage = new Datausage(req.body);
  console.info("Aggiornamento contatore in corso:", newDataUsage);

  let id = req.body._id;
  Datausage.findByIdAndUpdate(id, newDataUsage)
    .then((result) => {
      res.end();
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.eliminaDataUsage = (req, res) => {
  let DataUsageToDelete = req.body._id;
  console.info("Eliminazione contatore in corso:", DataUsageToDelete);

  Datausage.deleteOne({ _id: DataUsageToDelete })
    .then((result) => {
      if (result.deletedCount === 0) {
        res.send("La contatore da eliminare non è presente");
      } else {
        res.end();
      }
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.getNumShows = (req, res) => {
  Datausage.countDocuments()
    .then((result) => {
      res.status(200).send((result).toString());
    })
    .catch((err) => console.log("Errore conteggio Shows", err));
};
