const mongoose = require("mongoose");
const Prodotto = mongoose.model("Prodotto");
const Modello = mongoose.model("Modello");
const Categoria = mongoose.model("Categoria");
const Richiesta = mongoose.model("Richiesta");
const Datausage = mongoose.model("Datausage");
const _ = require("lodash");
var cloudinary = require("cloudinary").v2;
var formidable = require("formidable");
var fs = require("fs");

cloudinary.config({
  cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
});

//dato un id categoria, restituisce tutti i figli
exports.getDataTreeView = (req, res) => {
  console.info("Estrazione treeView in corso");
  let catid = req.query.id;

  Modello.find({ categoria: mongoose.Types.ObjectId(catid) })
    .sort({importanza: 1})
    .collation({ locale: "en_US", numericOrdering: true })
    .then((result) => {
      let modelsArray = _.map(result, (el) => {
        return { _id: el._id, modello: el.name };
      });
      console.log("modelli", modelsArray);

      Prodotto.aggregate([
        {
          $lookup: {
            from: "Modello",
            localField: "modello",
            foreignField: "_id",
            as: "modello",
          },
        },
        {
          $unwind: {
            path: "$modello",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $match: {
            "modello.categoria": mongoose.Types.ObjectId(catid),
          },
        },
      ])
        .exec()
        .then((result) => {
          console.log("estrazione", result);
          let toTree = [];

          _.forEach(modelsArray, (el) => {
            let obj = {};
            obj.id = el._id;
            obj.label = el.modello;
            obj.type = "modello";

            let tmpProducts = _.map(
              _.filter(result, (m) => {
                return m.modello.name === el.modello;
              }),
              (elm) => {
                return {
                  label: elm.name + (elm.size ? " - " + elm.size : " "),
                  id: elm._id,
                  type: "prodotto",
                };
              }
            );

            obj.children = tmpProducts;
            toTree.push(obj);
          });
          console.log("invio", toTree);

          res.status(200).send(toTree);
        })
        .catch((err) => res.status(500).send("Errore CRUD db"));
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.cloudUpload = (req, res) => {
  const form = new formidable.IncomingForm();
  form.parse(req);

  form.on("error", (err) => {
    res.status(500).send(err);
  });

  form.on("file", async (name, file) => {
    if (!fs.existsSync(file.path)) {
      let err = new Error(
        `The file '${file.path}' does not exist or is not accessible.`
      );
      console.error(err);
      res.status(500).send(err);
      return;
    }

    const cloudinary_bucket = process.env.VUE_APP_CLOUDINARY_BUCKET;

    //   fs.readFile(file.path, (e, fileData) => {
    cloudinary.uploader
      .upload(file.path, { folder: cloudinary_bucket })
      .then(function (image) {
        console.log();
        console.log("File Upload on Cloudinary service");
        console.log(image.public_id);
        console.log(image.url);
        res.status(200).send(image);
      })
      .catch(function (err) {
        console.log("Error on Cloudinary service", err);
        res.status(500).send(err);
      });
    //  });
  });
};

//estrazione dati categoria con models
exports.getDataCategory = (req, res) => {
  console.info("Estrazione category with models");
  let catid = req.query.id;

  Categoria.find({ _id: catid })
    .then((catResult) => {
      Modello.aggregate([
        {
          $match: {
            categoria: mongoose.Types.ObjectId(catid),
          },
        },
        {
          $lookup: {
            from: "Categoria",
            localField: "categoria",
            foreignField: "_id",
            as: "categoria",
          },
        },
        {
          $unwind: {
            path: "$categoria",
            preserveNullAndEmptyArrays: true,
          },
        },
      ]).sort({importanza: 1}) //ordina l'array dei modelli per importanza per il carosello sul sito
        .exec()
        .then((modelsResult) => {
          console.log("estrazione", modelsResult);

          let categoria = catResult[0]._doc;
          let tmp = [];
          _.forEach(modelsResult, (model) => {
            let curModello = new Modello(model);
            /* tmp.push({
              _id: model._id,
              importanza: model.importanza,
              slug: mode
              name: model.name,
              imgproducts: model.imgproducts,
              abstract: model.abstract,
            }); */
            tmp.push(curModello);
          });

          categoria.models = tmp;
          console.log("estrazione cat", categoria);
          res.status(200).send(categoria);
        })
        .catch((err) => res.status(500).send("Errore CRUD db"));
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.getDocumentReportData = (req, res) => {
  Richiesta.aggregate([
    {
      $group: {
        _id: {
          file: "$file",
          type: "$type",
          typename: "$typename",
          elementId: "$elementId",
          categoryId: "$categoryId",
        },
        count: {
          $sum: 1.0,
        },
      },
    },
    {
      $project: {
        file: "$_id.file",
        type: "$_id.type",
        typename: "$_id.typename",
        elementId: "$_id.elementId",
        categoryId: "$_id.categoryId",
        count: 1,
        _id: 0,
      },
    },
  ])
    .exec()
    .then((aggrResult) => {
      console.log("[Document report aggregate]", aggrResult);

      getDocuments(aggrResult)
        .then((arrayResult) => {
          console.log("ora fa res");
          res.status(200).send(arrayResult);
        })
        .catch((err) => console.log("Error aggr DocumentReport1", err));
    })
    .catch((err) => console.log("Error aggr DocumentReport2", err));
};

exports.getDocumentReportDataDetails = (req, res) => {
  let elId = req.query.id;
  let filename = req.query.file;
  Richiesta.find({
    elementId: elId,
    file: filename,
  })
    .then((result) => {
      console.log("result", result);
      res.status(200).send(result);
    })
    .catch((err) => console.log("Error aggr DocumentReportDetails", err));
};

function getDocuments(array) {
  return new Promise((resolve, reject) => {
    let arrayResult = [];
    console.log("Inizio getDocuments", array);

    Promise.all(
      array.map(async (el) => {
        let object = {};
        object.document = el.file;
        object.association = el.typename;
        object.requests = el.count;
        object.elementId = el.elementId;
        object.categoryId = el.categoryId;
        object.mdate = el.mdate;
        object.is_archive = await scanDocuments(el.type, el.elementId, el.file);
        arrayResult.push(object);
      })
    )
      .then((val) => {
        console.log("Fine getDocuments", arrayResult);
        resolve(arrayResult);
      })
      .catch((err) => console.log("err", err));
  });
}

function scanDocuments(type, elementId, file) {
  return new Promise((resolve, reject) => {
    if (type === "categoria") {
      Categoria.findById(elementId)
        .lean()
        .then((resp) => {
          let documents = resp.documents;
          let index = _.findIndex(documents, function (o) {
            return o.file === file;
          });
          if (!_.isNil(index) && index !== -1) {
            let isArchive = documents[index].archive
              ? documents[index].archive
              : false;
            resolve(isArchive);
          } else resolve(false);
        })
        .catch((err) => {
          console.log("err scan", err);
          reject();
        });
    } else if (type === "modello") {
      Modello.findById(elementId)
        .lean()
        .then((resp) => {
          let documents = resp.documents;
          let index = _.findIndex(documents, function (o) {
            return o.file === file;
          });
          if (!_.isNil(index) && index !== -1) {
            //console.log("questo 296", documents[index]);
            let isArchive = documents[index].archive
              ? documents[index].archive
              : false;
            resolve(isArchive);
          } else resolve(false);
        })
        .catch((err) => {
          console.log("err scan", err);
          reject();
        });
    } else if (type === "prodotto") {
      Prodotto.findById(elementId)
        .lean()
        .then((resp) => {
          let documents = resp.documents;
          let index = _.findIndex(documents, function (o) {
            return o.file === file;
          });
          if (!_.isNil(index) && index !== -1) {
            let isArchive = documents[index].archive
              ? documents[index].archive
              : false;
            resolve(isArchive);
          } else resolve(false);
        })
        .catch((err) => {
          console.log("err scan", err);
          reject();
        });
    }
  });
}

exports.getUserReportData = (req, res) => {
  Richiesta.aggregate([
    {
      $group: {
        _id: {
          email: "$email",
          username: "$username",
          company: "$company",
        },
        count: {
          $sum: 1.0,
        },
      },
    },
    {
      $project: {
        email: "$_id.email",
        username: "$_id.username",
        company: "$_id.company",
        count: 1,
        _id: 0,
      },
    },
  ])
    .exec()
    .then((userResult) => {
      Richiesta.find()
        .sort({ mdate: -1 })
        .then((allResults) => {
          console.log("all results", allResults);
          let arrayResult = [];
          _.forEach(userResult, (el) => {
            let arr = _.filter(allResults, (el2) => {
              return el2.email === el.email;
            });
            let obj = {};
            obj.email = el.email;
            obj.username = el.username;
            obj.count = el.count;
            obj.company = el.company;
            obj.phone = arr[0].phone;
            arrayResult.push(obj);
          });
          res.status(200).send(arrayResult);
        });
    })
    .catch((err) => console.log("Error aggr UserReport", err));
};

exports.getUserReportDataDetail = (req, res) => {
  let email = req.query.email;
  Richiesta.aggregate([
    {
      $group: {
        _id: {
          email: "$email",
          username: "$username",
          company: "$company",
          phone: "$phone",
          file: "$file",
          typename: "$typename",
          type: "$type",
          elementId: "$elementId",
          mdate: "$mdate",
        },
        count: {
          $sum: 1.0,
        },
      },
    },
    {
      $project: {
        email: "$_id.email",
        username: "$_id.username",
        company: "$_id.company",
        phone: "$_id.phone",
        file: "$_id.file",
        typename: "$_id.typename",
        type: "$_id.type",
        elementId: "$_id.elementId",
        mdate: "$_id.mdate",
        count: 1,
        _id: 0,
      },
    },
    {
      $match: {
        email: email,
      },
    },
  ])
    .exec()
    .then((aggrResult) => {
      console.log("aggrresult", aggrResult);

      getDocuments(aggrResult)
        .then((arrayResult) => {
          console.log("ora fa res");
          res.status(200).send(arrayResult);
        })
        .catch((err) => console.log("Error aggr UserReport1", err));
    })
    .catch((err) => console.log("Error aggr UserReport2", err));
};

function getProdForSyS() {
  return Richiesta.aggregate([
    {
      $group: {
        _id: {
          elementId: "$elementId",
          typename: "$typename",
        },
        requests: {
          $sum: 1.0,
        },
      },
    },
    {
      $project: {
        productId: "$_id.elementId",
        product: "$_id.typename",
        requests: "$requests",
        _id: 0,
      },
    },
  ]);
}

function getRequestsForSys() {
  return Datausage.aggregate([
    {
      $group: {
        _id: {
          productId: "$productId",
          product: "$product",
          category: "$category",
        },
        count: {
          $sum: 1.0,
        },
      },
    },
    {
      $project: {
        productId: "$_id.productId",
        product: "$_id.product",
        category: "$_id.category",
        count: 1,
        _id: 0,
      },
    },
  ]);
}

exports.getDataUsageSys = (req, res) => {
  var arrayResult = [];
  arrayResult.push(getRequestsForSys());
  arrayResult.push(getProdForSyS());

  //console.log("arrayResult: ", arrayResult);

  Promise.all(arrayResult)
    .then((results) => {
      let newArray = [];
      let arrayRequests = results[0];
      let arrayCount = results[1];
      _.forEach(arrayRequests, (elReq) => {
        let obj = {};
        obj.product = elReq.product;
        obj.category = elReq.category;
        obj.count = elReq.count;
        obj.requests = 0;

        _.forEach(arrayCount, (elCount) => {
          if (elReq.product === elCount.product) {
            obj.requests = elCount.requests;
          }
        });

        newArray.push(obj);
      });
      console.log("results: ", newArray);
      res.status(200).send(newArray);
    })
    .catch((err) => console.log("Error", err));
};

function baseCustomProperties(catid) {
  return new Promise((resolve, reject) => {
    Prodotto.aggregate([
      {
        $lookup: {
          from: "Modello",
          localField: "modello",
          foreignField: "_id",
          as: "modello",
        },
      },
      {
        $unwind: {
          path: "$modello",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $match: {
          "modello.categoria": mongoose.Types.ObjectId(catid),
        },
      },
    ])
      .exec()
      .then((prodotti) => {
        console.log("estrazione", prodotti);
        resolve(prodotti);
      })
      .catch((err) => {
        console.log("Errore CRUD db", err);
        reject();
      });
  });
}

function toJson(array) {
  let json = {};
  _.forEach(array, function (el) {
    json[el.property] = el.value;
  });
  return json;
}

function getRange(obj) {
  let json = {};
  let arr = obj.split(",");
  json.min = arr[0];
  json.max = arr[1];
  return json;
}

//SITO - CONFIGURATORE FWG
/* 
parametri:
  water,
  temperature,
  selectedType,

cosa configurare lato prodotto:
  water_range1: value,
  temperature_range1: value,
  ...
  water_rangeN: value,
  temperature_rangeN: value,
  water_type: seawater o feedwater (seawater -> C-Type, feedwater -> S-Type)
*/
exports.configuratoreFWG = (req, res) => {
  let params = {
    id: req.query.catId,
    water: req.query.water,
    temperature: req.query.temperature,
    waterType: req.query.waterType,
  };

  let prodotti = baseCustomProperties(params.id)
    .then((result) => {
      console.log("risultati", result);
      let arrayResult = [];

      //qui logiche configuratore
      _.forEach(result, function (obj) {
        let json = toJson(obj.other);
        console.log("json inside", json);

        for (let i = 1; i <= obj.other.length; i++) {
          let propWater = "water_range" + i;
          let propTemperature = "temperature_range" + i;
          if (json[propWater] && json[propTemperature]) {
            let wrange = getRange(json[propWater]);
            let trange = getRange(json[propTemperature]);
            if (
              parseInt(params.water) >= parseInt(wrange.min) &&
              parseInt(params.water) <= parseInt(wrange.max) &&
              parseInt(params.temperature) >= parseInt(trange.min) &&
              parseInt(params.temperature) <= parseInt(trange.max)
            ) {
              if (
                params.waterType === json.water_type ? json.water_type : null
              ) {
                arrayResult.push(obj);
              }
            }
          } else if (json[propWater]) {
            let wrange = getRange(json[propWater]);
            if (
              parseInt(params.water) >= parseInt(wrange.min) &&
              parseInt(params.water) <= parseInt(wrange.max)
            ) {
              arrayResult.push(obj);
            }
          }
        }
      });

      let objectToSend = null;

      if (arrayResult.length > 1) {
        _.forEach(arrayResult, function (obj) {
          if (objectToSend === null) {
            objectToSend = obj;
          } else {
            if (parseInt(obj.size) < parseInt(objectToSend.size))
              objectToSend = obj;
          }
        });
      } else if (arrayResult.length === 1) {
        objectToSend = arrayResult[0];
      } else objectToSend = {};

      res.status(200).send(objectToSend);
    })
    .catch((err) => {
      res.status(500).send("Errore CRUD db");
    });
};

//SITO - CONFIGURATORE BWT
/* 
parametri:
  flowrate,
  explosion, (optional) yes/no
  componentType,(optional) loose/skid
  coolingMode, (optional) cabinet/air  

cosa configurare lato prodotto:
  min_flowrate: value,
  max_flowrate: value,
  component_type: value,
  cooling_mode: value,
  explosion: value
*/
exports.configuratoreBWT = (req, res) => {
  let params = {
    id: req.query.catId,
    flowrate: req.query.flowrate,
    componentType: req.query.componentType ? req.query.componentType : null,
    coolingMode: req.query.coolingMode ? req.query.coolingMode : null,
    explosion: req.query.explosion ? req.query.explosion : null,
  };

  let prodotti = baseCustomProperties(params.id)
    .then((result) => {
      console.log("risultati", result);
      let arrayResult = [];

      //qui logiche configuratore
      _.forEach(result, function (obj) {
        let json = toJson(obj.other);
        console.log("json inside", json);

        if (
          parseInt(params.flowrate) >= parseInt(json.min_flowrate) &&
          parseInt(params.flowrate) <= parseInt(json.max_flowrate)
        ) {
          if (params.explosion === (json.explosion ? json.explosion : null)) {
            if (params.explosion === "yes") {
              arrayResult.push(obj);
            } else {
              if (
                params.componentType ===
                (json.component_type ? json.component_type : null)
              ) {
                if (
                  params.coolingMode ===
                  (json.cooling_mode ? json.cooling_mode : null)
                ) {
                  arrayResult.push(obj);
                }
              }
            }
          }

          /* if (
            params.componentType ===
            (json.component_type ? json.component_type : null)
          ) {
            if (
              params.coolingMode ===
              (json.cooling_mode ? json.cooling_mode : null)
            ) {
              if (
                params.explosion === (json.explosion ? json.explosion : null)
              ) {
                arrayResult.push(obj);
              }
            }
          } */
        }
      });

      let objectToSend = null;

      if (arrayResult.length > 1) {
        _.forEach(arrayResult, function (obj) {
          if (objectToSend === null) {
            objectToSend = obj;
          } else {
            if (parseInt(obj.size) < parseInt(objectToSend.size))
              objectToSend = obj;
          }
        });
      } else if (arrayResult.length === 1) {
        objectToSend = arrayResult[0];
      } else objectToSend = {};

      res.status(200).send(objectToSend);
    })
    .catch((err) => {
      res.status(500).send("Errore CRUD db");
    });
};

//SITO - CONFIGURATORE BWS
/* 
parametri:
  flowrate,
  concentration

cosa configurare lato prodotto:
  min_flowrate: value,
  max_flowrate: value,
  min_concentration: value,
  max_concentration: value,
*/
exports.configuratoreBWS = (req, res) => {
  let params = {
    id: req.query.catId,
    flowrate: req.query.flowrate,
    concentration: req.query.concentration,
  };

  let prodotti = baseCustomProperties(params.id)
    .then((result) => {
      console.log("risultati", result);
      let arrayResult = [];

      //qui logiche configuratore
      _.forEach(result, function (obj) {
        let json = toJson(obj.other);
        console.log("json inside", json);

        if (
          parseInt(params.flowrate) >= parseInt(json.min_flowrate) &&
          parseInt(params.flowrate) <= parseInt(json.max_flowrate)
        ) {
          if (
            parseInt(params.concentration) >=
            parseInt(json.min_concentration) &&
            parseInt(params.concentration) <= parseInt(json.max_concentration)
          ) {
            arrayResult.push(obj);
          }
        }
      });

      let objectToSend = null;

      if (arrayResult.length > 1) {
        _.forEach(arrayResult, function (obj) {
          if (objectToSend === null) {
            objectToSend = obj;
          } else {
            if (parseInt(obj.size) < parseInt(objectToSend.size))
              objectToSend = obj;
          }
        });
      } else if (arrayResult.length === 1) {
        objectToSend = arrayResult[0];
      } else objectToSend = {};

      res.status(200).send(objectToSend);
    })
    .catch((err) => {
      res.status(500).send("Errore CRUD db");
    });
};

//SITO - CONFIGURATORE HSS
/* 
parametri:
  rootType: ('lubricant','fuel')
  cleaning: ('self', 'manual'),
  fuelType:('hfo', 'mdo', 'go'),
  lubrificantType: ('hfo', 'mdo', 'go'),
  certified: yes/no,
  isConsumptionFuel: yes/no,
  isConsumptionLubrificant: yes/no,
  fuelConsumption: value,
  fuelPower: value,
  lubrificantConsumption: value,
  lubrificantPower: value,
  powerUnit: ('kw', 'hp'),

cosa configurare lato prodotto:
  cleaning: 'manual','self',
  certifiedFlowRateHFO: value,
  productFlowRateHFO: value,
  productFlowRateMDO: value,
  productFlowRateGO: value,
  productFlowRateLO: value,
  category: 'p','s'
*/
exports.configuratoreHSS = (req, res) => {
  let params = {
    id: req.query.catId,
    rootType: req.query.rootType,
    cleaning: req.query.cleaning,
    fuelType: req.query.fuelType,
    lubrificantType: req.query.lubrificantType,
    certified: req.query.certified,
    isConsumptionFuel: req.query.isConsumptionFuel,
    isConsumptionLubrificant: req.query.isConsumptionLubrificant,
    fuelConsumption: req.query.fuelConsumption ? req.query.fuelConsumption : null,
    fuelPower: req.query.fuelPower ? req.query.fuelPower : null,
    lubrificantConsumption: req.query.lubrificantConsumption ? req.query.lubrificantConsumption : null,
    lubrificantPower: req.query.lubrificantPower ? req.query.lubrificantPower : null,
    powerUnit: req.query.powerUnit,
  };

  let prodotti = baseCustomProperties(params.id)
    .then((result) => {
      console.log("risultati", result);
      let arrayResult = [];

      //qui logiche configuratore
      _.forEach(result, function (obj) {
        let json = toJson(obj.other);
        console.log("json inside", json);

        // limito lista prodotti a prodotti compatibili per fluido e certificazione caso fuel
        if (params.rootType === 'fuel') {
          if (params.cleaning === json.cleaning) {
            if (params.fuelConsumption) {
              if (params.fuelType === 'hfo') {
                if (params.certified === 'yes') {
                  if(json.certifiedFlowRateHFO){
                    if (Math.floor(json.certifiedFlowRateHFO / params.fuelConsumption) > 0) { arrayResult.push(obj); }
                  }
                }
                else {
                  if (Math.floor(json.productFlowRateHFO / params.fuelConsumption) > 0) { arrayResult.push(obj); }
                }
              }
              else if (params.fuelType === 'mdo') {
                if (Math.floor(json.productFlowRateMDO / params.fuelConsumption) > 0) {
                  if(json.cleaning === 'self'){
                    if(params.fuelConsumption > 9000){arrayResult.push(obj);} 
                    else if(params.fuelConsumption <= 9000 && json.category === 'p') {arrayResult.push(obj);}
                  } else {
                    if(params.fuelConsumption > 3900){arrayResult.push(obj);}
                      else if(params.fuelConsumption <= 3900 && obj.size != '206'){arrayResult.push(obj);}
                    }
                }
              }
              else {
                if (Math.floor(json.productFlowRateGO / params.fuelConsumption) > 0) {
                  if(json.cleaning === 'self'){ 
                    if(params.fuelConsumption > 9500){arrayResult.push(obj);} 
                    else if(params.fuelConsumption <= 9500 && json.category === 'p') {arrayResult.push(obj);}
                  } else {                    
                    if(params.fuelConsumption > 4700){arrayResult.push(obj);}
                    else if(params.fuelConsumption <= 4700 && obj.size != '206'){arrayResult.push(obj);}
                  }
                }
              }
            } else { /*calcolo la portata se non me la da' l'utente*/
              if (params.cleaning === 'manual') {
                if (params.powerUnit === 'kw') {
                  params.fuelConsumption = 0.248 * params.fuelPower;
                }
                else { params.fuelConsumption = 0.183 * params.fuelPower; }
              } else {
                if (params.powerUnit === 'kw') {
                  params.fuelConsumption = 0.238 * params.fuelPower;
                } else { params.fuelConsumption = 0.175 * params.fuelPower; }
              }

              if (params.fuelType === 'hfo') {
                if (Math.floor(json.productFlowRateHFO / params.fuelConsumption) > 0) { arrayResult.push(obj); }
              }
              else if (params.fuelType === 'mdo') {
                if (Math.floor(json.productFlowRateMDO / params.fuelConsumption) > 0) {
                  if(json.cleaning === 'self'){
                    if(params.fuelConsumption > 9000){arrayResult.push(obj);} 
                    else if(params.fuelConsumption <= 9000 && json.category === 'p') {arrayResult.push(obj);}
                  } else {                    
                    if(params.fuelConsumption > 3900){arrayResult.push(obj);}
                    else if(params.fuelConsumption <= 3900 && obj.size != '206'){arrayResult.push(obj);}
                }
                 }
              }
              else {
                if (Math.floor(json.productFlowRateGO / params.fuelConsumption) > 0) {
                  if(json.cleaning === 'self'){
                    if(params.fuelConsumption > 9500){arrayResult.push(obj);} 
                    else if(params.fuelConsumption <= 9500 && json.category === 'p') {arrayResult.push(obj);}
                  } else {
                      if(params.fuelConsumption > 4700){arrayResult.push(obj);}
                      else if(params.fuelConsumption <= 4700 && obj.size != '206'){arrayResult.push(obj);}
                  }
                 }
              }
            }
          }
        }
        else { /*come sopra nel caso in cui il rootType sia lubricant e non fuel*/
          if (params.cleaning === json.cleaning) {
            if (params.lubrificantConsumption) {
              if (Math.floor(json.productFlowRateLO / params.lubrificantConsumption) > 0) { 
                  if(json.cleaning === 'self'){arrayResult.push(obj);}
                  else {                      
                    if(params.lubrificantConsumption > 1300){arrayResult.push(obj);}
                    else if(params.lubrificantConsumption <=1300 && obj.size != '206'){arrayResult.push(obj);}
                  }
                }
            } else { /*calcolo la portata se non me la da' l'utente*/
              if (params.cleaning === 'manual') {
                if (params.lubrificantType === 'mdo') {
                  if (params.powerUnit === 'kw') { params.lubrificantConsumption = 0.40 * params.lubrificantPower; }
                  else { params.lubrificantConsumption = 0.30 * params.lubrificantPower; }
                }
                else {
                  if (params.powerUnit === 'kw') { params.lubrificantConsumption = 0.27 * params.lubrificantPower; }
                  else { params.lubrificantConsumption = 0.20 * params.lubrificantPower; }
                }
              }
              else {
                if (params.lubrificantType === 'hfo') {
                  if (params.powerUnit === 'kw') { params.lubrificantConsumption = 0.34 * params.lubrificantPower; }
                  else { params.lubrificantConsumption = 0.25 * params.lubrificantPower; }
                }
                else if (params.lubrificantType === 'mdo') {
                  if (params.powerUnit === 'kw') { params.lubrificantConsumption = 0.34 * params.lubrificantPower; }
                  else { params.lubrificantConsumption = 0.25 * params.lubrificantPower; }
                }
                else {
                  if (params.powerUnit === 'kw') { params.lubrificantConsumption = 0.23 * params.lubrificantPower; }
                  else { params.lubrificantConsumption = 0.17 * params.lubrificantPower; }
                }
              }
              if (Math.floor(json.productFlowRateLO / params.lubrificantConsumption) > 0) {                   
                if(json.cleaning === 'self'){arrayResult.push(obj);}
                else {                      
                  if(params.lubrificantConsumption > 1300){arrayResult.push(obj);}
                  else if(params.lubrificantConsumption <=1300 && obj.size != '206'){arrayResult.push(obj);}
              }}
            }
          }
        }
      }); /*chiudo ciclo for*/

      let objectToSend = null;

      if (arrayResult.length > 1) {
        _.forEach(arrayResult, function (obj) {
          if (objectToSend === null) {
            objectToSend = obj;
          } else {
            let arrSplit = obj.size.split(' ');
            if (arrSplit.length > 1){
              let numb = obj.size.split(' ')[1];
              let numbToSend = objectToSend.size.split(' ')[1];
              if (parseInt(numb) < parseInt(numbToSend))
                objectToSend = obj;
            } else {
              if (parseInt(obj.size) < parseInt(objectToSend.size))
                objectToSend = obj;
            }
          }
        });
      } else if (arrayResult.length === 1) {
        objectToSend = arrayResult[0];
      } else objectToSend = {};

      res.status(200).send(objectToSend);
    })
    .catch((err) => {
      res.status(500).send("Errore CRUD db");
    });
};

function getNumDocumentsCat() {
  return Categoria.aggregate([
    {
      $project: {
        numberOfLinks: {
          $size: {
            $filter: {
              input: "$documents",
              as: "doc",
              cond: {
                $eq: ["$$doc.link", true],
              },
            },
          },
        },

        numberOfFiles: {
          $size: {
            $filter: {
              input: "$documents",
              as: "doc2",
              cond: {
                $or: [
                  { $eq: ["$$doc2.link", false] },
                  { $lt: ["$$doc2.link", null] }, //serve per controllare se un field esiste all'interno di un aggregate
                ],
              },
            },
          },
        },
        numberTotal: {
          $size: "$documents",
        },
      },
    },
  ]);
}
function getNumDocumentsMod() {
  return Modello.aggregate([
    {
      $project: {
        numberOfLinks: {
          $size: {
            $filter: {
              input: "$documents",
              as: "doc",
              cond: {
                $eq: ["$$doc.link", true],
              },
            },
          },
        },

        numberOfFiles: {
          $size: {
            $filter: {
              input: "$documents",
              as: "doc2",
              cond: {
                $or: [
                  { $eq: ["$$doc2.link", false] },
                  { $lt: ["$$doc2.link", null] }, //serve per controllare se un field esiste all'interno di un aggregate
                ],
              },
            },
          },
        },
        numberTotal: {
          $size: "$documents",
        },
      },
    },
  ]);
}
function getNumDocumentsProd() {
  return Prodotto.aggregate([
    {
      $project: {
        numberOfLinks: {
          $size: {
            $filter: {
              input: "$documents",
              as: "doc",
              cond: {
                $eq: ["$$doc.link", true],
              },
            },
          },
        },

        numberOfFiles: {
          $size: {
            $filter: {
              input: "$documents",
              as: "doc2",
              cond: {
                $or: [
                  { $eq: ["$$doc2.link", false] },
                  { $lt: ["$$doc2.link", null] }, //serve per controllare se un field esiste all'interno di un aggregate
                ],
              },
            },
          },
        },
        numberTotal: {
          $size: "$documents",
        },
      },
    },
  ]);
}

exports.getDocumentsData = (req, res) => {
  var arrayResult = [];
  arrayResult.push(getNumDocumentsCat());
  arrayResult.push(getNumDocumentsMod());
  arrayResult.push(getNumDocumentsProd());

  //console.log("Inizio getNumDocuments", arrayResult);
  let counterFile = 0;
  let counterLink = 0;

  Promise.all(arrayResult)
    .then((results) => {
      //console.log("risultati: ", results);
      let files = [];
      let links = [];

      _.forEach(results, (el) => {
        let numberOfFiles = el.numberOfFiles;
        let numberOfLinks = el.numberOfLinks;

        let countTotal = _.forEach(el, (element) => {
          return {
            numberOfLinks: element.numberOfLinks,
            numberOfFiles: element.numberOfFiles,
          };
        });

        let totalFiles = countTotal.reduce(function (accumulator, currentValue) {
          return accumulator + currentValue.numberOfFiles;
        }, counterFile);

        let totalLinks = countTotal.reduce(function (accumulator, currentValue) {
          return accumulator + currentValue.numberOfLinks;
        }, counterLink);

        files.push(totalFiles);
        links.push(totalLinks);
      });

      let obj = {};
      obj.numberOfFiles = files.reduce((a, b) => a + b, 0);
      obj.numberOfLinks = links.reduce((a, b) => a + b, 0);

      res.status(200).send(obj);
    })
    .catch((err) => console.log("Error", err));
};
