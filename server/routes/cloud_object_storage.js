/* eslint-disable no-useless-escape */
var ibm = require("ibm-cos-sdk");
var formidable = require("formidable");
var fs = require("fs");
const async = require("async");

var config = {
  endpoint: process.env.COS_ENDPOINT,
  apiKeyId: process.env.COS_API_KEY,
  ibmAuthEndpoint: process.env.COS_AUTH_ENDPOINT,
  serviceInstanceId: process.env.COS_SERVICE_ID
};

const cos = new ibm.S3(config);
const myBucket = process.env.COS_BUCKET; //"alfalaval-config";

// Retrieve a particular item from the bucket
exports.getItem = async (req, res) => {
  const itemName = req.query.filename;
  console.log("Ricerca file ", req, itemName);
  console.log(`Retrieving item from bucket: ${myBucket}, key: ${itemName}`);
  // return headFile(itemName)
  const signed = await signedUrlDownload(itemName)
  res.status(200).send(signed)
};

exports.deleteItem = async (req, res) => {
  let itemName = req.query.filename;
  console.log("Cancellazione file ", req, itemName);
  console.log("Deleting object");
  const response = await deleteFile(itemName)
  res.status(200).send(response)
};

// Multi part upload
exports.upload = async (req, res) => {
  // restituisco solo la signed url, l'upload lo fa q-uploader sul client
  const signed = await signedUrl(req.body.percorso, req.body.tipo)
  res.status(200).send(signed)
};

function multiPartUpload(itemName, contentType, filePath) {
  return new Promise((resolve, reject) => {
    var bucketName = myBucket;
    var uploadID = null;

    console.log(
      `Starting multi-part upload for ${itemName} to bucket: ${bucketName}`
    );
    cos
      .createMultipartUpload({
        Bucket: bucketName,
        Key: itemName,
        ContentType: contentType,
        Metadata: {
          filename: itemName,
        }
      })
      .promise()
      .then((data) => {
        uploadID = data.UploadId;

        //begin the file upload
        fs.readFile(filePath, (e, fileData) => {
          //min 5MB part
          var partSize = 1024 * 1024 * 5;
          var partCount = Math.ceil(fileData.length / partSize);

          async.timesSeries(
            partCount,
            (partNum, next) => {
              var start = partNum * partSize;
              var end = Math.min(start + partSize, fileData.length);

              partNum++;

              console.log(
                `Uploading to ${itemName} (part ${partNum} of ${partCount})`
              );

              cos
                .uploadPart({
                  Body: fileData.slice(start, end),
                  Bucket: bucketName,
                  Key: itemName,
                  PartNumber: partNum,
                  UploadId: uploadID,
                })
                .promise()
                .then((data) => {
                  next(e, { ETag: data.ETag, PartNumber: partNum });
                })
                .catch((e) => {
                  cancelMultiPartUpload(bucketName, itemName, uploadID);
                  console.error(`ERROR: ${e.code} - ${e.message}\n`);
                });
            },
            (e, dataPacks) => {
              cos
                .completeMultipartUpload({
                  Bucket: bucketName,
                  Key: itemName,
                  MultipartUpload: {
                    Parts: dataPacks,
                  },
                  UploadId: uploadID,
                })
                .promise()
                .then(() => {
                  console.log(
                    `Upload of all ${partCount} parts of ${itemName} successful.`
                  );
                  resolve();
                })
                .catch((e) => {
                  cancelMultiPartUpload(bucketName, itemName, uploadID);
                  console.error(`ERROR: ${e.code} - ${e.message}\n`);
                  reject();
                });
            }
          );
        });
      })
      .catch((e) => {
        console.error(`ERROR: ${e.code} - ${e.message}\n`);
        reject();
      });
  });
}

function cancelMultiPartUpload(bucketName, itemName, uploadID) {
  return cos
    .abortMultipartUpload({
      Bucket: bucketName,
      Key: itemName,
      UploadId: uploadID,
    })
    .promise()
    .then(() => {
      console.log(`Multi-part upload aborted for ${itemName}`);
    })
    .catch((e) => {
      console.error(`ERROR: ${e.code} - ${e.message}\n`);
    });
}
let signedUrlDownload, signedUrl, deleteFile
const loadES6Module = async () => {
  const upload = await import('../lib/upload.mjs')
  signedUrlDownload = upload.signedUrlDownload
  signedUrl = upload.signedUrl
  deleteFile = upload.deleteFile
}

loadES6Module()