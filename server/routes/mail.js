const nodemailer = require("nodemailer");
const mandrillTransport = require("nodemailer-mandrill-transport");
const fs = require("fs");
const mustache = require("mustache");
const template = fs.readFileSync(
  process.cwd() + "/server/templates/email/contacts.html",
  "utf8"
);

var smtpTransport = nodemailer.createTransport(
  mandrillTransport({
    auth: {
      apiKey: process.env.MANDRILL_API_KEY,
    },
  })
);

exports.sendEmail = (req, res) => {
  let obj = req.body;
  let recipients = "";
  if (obj.type === "energy") {
    if (obj.mailType === "new") {
      obj.mailType = "New equipment";
      recipients = process.env.MANDRILL_MAIL_LIST_NEW_ENERGY;
    } else {
      obj.mailType = "Technical assistance";
      recipients = process.env.MANDRILL_MAIL_LIST_TECH_ENERGY;
    }
  } else {
    if (obj.mailType === "new") {
      obj.mailType = "New equipment";
      recipients = process.env.MANDRILL_MAIL_LIST_NEW_MARINE;
    } else {
      obj.mailType = "Technical assistance";
      recipients = process.env.MANDRILL_MAIL_LIST_TECH_MARINE;
    }
  }

  let myhtml = mustache.render(template, obj);

  let mailOptions = {
    from: process.env.MANDRILL_FROM,
    to: recipients,
    subject: "Message sent by " + obj.name,
    html: myhtml,
  };

  // Sending email.
  smtpTransport.sendMail(mailOptions, function(error, response) {
    if (error) {
      res.status(500).send("Err on sending mail");
      //throw new Error("Error in sending email");
    }
    console.log("Message sent: " + JSON.stringify(response));
    res.end();
  });
};
