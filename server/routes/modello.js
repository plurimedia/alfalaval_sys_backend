const mongoose = require("mongoose");
const Modello = mongoose.model("Modello");
const _ = require("lodash");

//CRUD Modello
exports.getAllModelli = (req, res) => {
  console.info("Estrazione modelli in corso");
  Modello.find()
    .then((results) => {
      res.status(200).send(results);
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.getModelloById = (req, res) => {
  console.info("Estrazione modello in corso");
  let id = req.query.id;
  Modello.find({ _id: id })
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.getModelloByName = (req, res) => {
  console.info("Estrazione modello in corso");
  let name = req.query.name;
  Modello.find({ name: name })
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.saveModello = (req, res) => {
  let newModello = new Modello(req.body);
  console.info("Salvataggio modello in corso:", newModello);

  newModello
    .save()
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.updateModello = (req, res) => {
  let newModello = new Modello(req.body);
  console.info("Aggiornamento modello in corso:", newModello);

  let id = req.body._id;
  Modello.findByIdAndUpdate(id, newModello)
    .then((result) => {
      res.end();
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.updateDocModello = (req, res) => {
  //recuperare i documents
  let id = req.query.id;
  let row = req.body; //c'è il singolo document
  row.mdate = new Date(); //aggiorno data modifica

  Modello.find({ _id: id })
    .lean()
    .then((result) => {
      //updatare il doc corrispondente
      let newModello = result[0];
      let toUpdate = newModello.documents.map((el) => {
        if (el.name === row.name && el.file === row.file) el = row;
        return el;
      });

      newModello.documents = toUpdate;
      console.info("Aggiornamento modello in corso:", newModello);

      Modello.findByIdAndUpdate(id, newModello)
        .then((result) => {
          res.end();
        })
        .catch((err) => res.status(500).send("Errore CRUD db"));
    })
    .catch((err) => {
      console.log("Error modello Update", err);
      res.status(500).send("Errore CRUD db");
    });
};

exports.eliminaModello = (req, res) => {
  let ModelloToDelete = req.body._id;
  console.info("Eliminazione modello in corso:", ModelloToDelete);

  Modello.deleteOne({ _id: ModelloToDelete })
    .then((result) => {
      if (result.deletedCount === 0) {
        res.send("Il modello da eliminare non è presente");
      } else {
        res.end();
      }
    })
    .catch((err) => res.status(500).send("Errore CRUD db"));
};

exports.getNumModelli = (req, res) => {
  Modello.countDocuments()
    .then((result) => {
      res.status(200).send((result).toString());
    })
    .catch((err) => console.log("Errore conteggio Modelli", err));
};
