require("dotenv-flow").config();
const express = require("express");
const jwt = require("express-jwt");
const path = require("path");
const helmet = require("helmet");
const mongoose = require("mongoose");
const cors = require("cors");
const compression = require("compression");
const fs = require("fs");
//const multipart = require("connect-multiparty"); // per uplaod files

const categoriaSchema = require("./server/models/categoria");
const Categoria = require("./server/routes/categoria");
const modelloSchema = require("./server/models/modello");
const Modello = require("./server/routes/modello");
const prodottoSchema = require("./server/models/prodotto");
const Prodotto = require("./server/routes/prodotto");
const richiestaSchema = require("./server/models/richiesta");
const Richiesta = require("./server/routes/richiesta");
const dataUsageSchema = require("./server/models/datausage");
const Datausage = require("./server/routes/datausage");

const Cos = require("./server/routes/cloud_object_storage");
const Mail = require("./server/routes/mail");
const V1 = require("./server/routes/v1");

// Create a new Express app
const app = express();
app.use(express.json()); //body-parser
app.use(
  helmet({
    contentSecurityPolicy: false
  })
); //protegge da attacchi base
app.use(compression()); //riduce il peso del payload
app.use(express.static(path.join(process.cwd(), "dist"))); //importante per deploy
app.use(cors());

// **** Auth0 ****
const authConfig = {
  domain: process.env.VUE_APP_DOMAIN
};

const secret = process.env.AUTH0_SECRET;

const checkJwt = jwt({
  secret: secret,
  algorithms: ["HS256"]
});
// **** fine  ****

// const certFileDB = fs.readFileSync("./ibm_mongodb_ca.pem"); // Carica certificato SSL
// var mongoUrl;
// Controllo se le variabili di OpenShift sono popolate
/*if (
  process.env.MONGOUSERNAME &&
  process.env.MONGOPWD &&
  process.env.MONGOHOST &&
  process.env.MONGODBNAME
) {*/
var mongoUrl =
  "mongodb://" +
  process.env.MONGOUSERNAME +
  ":" +
  process.env.MONGOPWD +
  "@" +
  process.env.MONGOHOST +
  "/" +
  process.env.MONGODBNAME;

if (process.env.MONGOOPTIONS) {
  var mongoCliOptions = process.env.MONGOOPTIONS;
  // Quest'applicazione utilizza un container Docker custom, pertanto alcuni caratteri nelle variabili globali
  // vengono trascritti nel codice unicode ed è impossibile l'utilizzo del backslash (\) come funzione
  // di escape.
  mongoCliOptions = mongoCliOptions.replace(/\\u0026/g, "&");
  mongoUrl = mongoUrl + "?" + mongoCliOptions;
}
/*} else {
  mongoUrl = process.env.MONGOURL; // Variabile impostata nei config.js
}*/

mongoose.Promise = global.Promise;
var mongoOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
};
// sslValidate: true,
// sslCA: certFileDB

mongoose.connect(mongoUrl, mongoOptions, err => {
  if (err) throw err;

  console.info("### Connesso a: ", mongoUrl);
  console.info("### Ambiente: ", process.env.NODE_ENV);
});

// ******* ROUTES ********
// Categoria
app.get("/api/secure/categoria/all", checkJwt, Categoria.getAllCategorie);
app.get("/api/sito/categoria/all", Categoria.getAllCategorie);
app.get(
  "/api/secure/categoria/get-by-id",
  checkJwt,
  Categoria.getCategoriaById
); //:id param (non lo prende più con la vecchia notazione /categoria/get/:id)
app.get("/api/sito/categoria/get-by-id", Categoria.getCategoriaById);
app.get(
  "/api/secure/categoria/get-by-name",
  checkJwt,
  Categoria.getCategoriaByName
);
app.get("/api/secure/categoria/numCategorie", Categoria.getNumCategorie);
//:name param
app.post("/api/secure/categoria/save", checkJwt, Categoria.saveCategoria); //full object in body
app.put("/api/secure/categoria/update", checkJwt, Categoria.updateCategoria);
app.put(
  "/api/secure/categoria/update-doc",
  checkJwt,
  Categoria.updateDocCategoria
);
app.delete(
  "/api/secure/categoria/delete",
  checkJwt,
  Categoria.eliminaCategoria
);

// Modello
app.get("/api/secure/modello/all", checkJwt, Modello.getAllModelli);
app.get("/api/secure/modello/get-by-id", checkJwt, Modello.getModelloById);
app.get("/api/sito/modello/get-by-id", Modello.getModelloById); //:id param (non lo prende più con la vecchia notazione /categoria/get/:id)
app.get("/api/secure/modello/get-by-name", checkJwt, Modello.getModelloByName); //:name param
app.get("/api/secure/modello/numModello", Modello.getNumModelli);
app.post("/api/secure/modello/save", checkJwt, Modello.saveModello); //full object in body
app.put("/api/secure/modello/update", checkJwt, Modello.updateModello);
app.put("/api/secure/modello/update-doc", checkJwt, Modello.updateDocModello);
app.delete("/api/secure/modello/delete", checkJwt, Modello.eliminaModello);

//Prodotto
app.get("/api/secure/prodotto/all", checkJwt, Prodotto.getAllProdotti);
app.get("/api/secure/prodotto/get-by-id", checkJwt, Prodotto.getProdottoById);
app.get("/api/sito/prodotto/get-by-id", Prodotto.getProdottoById);
app.get(
  "/api/secure/prodotto/get-by-name",
  checkJwt,
  Prodotto.getProdottoByName
);
app.get("/api/secure/prodotto/numProdotto", Prodotto.getNumProdotti);
app.post("/api/secure/prodotto/save", checkJwt, Prodotto.saveProdotto);
app.put("/api/secure/prodotto/update", checkJwt, Prodotto.updateProdotto);
app.put(
  "/api/secure/prodotto/update-doc",
  checkJwt,
  Prodotto.updateDocProdotto
);
app.delete("/api/secure/prodotto/delete", checkJwt, Prodotto.eliminaProdotto);

//Richiesta
app.get("/api/richiesta/all", Richiesta.getAllRichieste);
app.get("/api/richiesta/get-by-id", Richiesta.getRichiestaById);
app.get("/api/richiesta/get-by-user", Richiesta.getRichiestaByUser);
app.get("/api/richiesta/numRichieste", Richiesta.getNumRichieste);
app.get("/api/richiesta/numRichiesteProd", Richiesta.getRichiesteProdotti);
app.get("/api/richiesta/numUsers", Richiesta.getNumUsers);
app.post("/api/richiesta/save", Richiesta.saveRichiesta); //
app.put("/api/richiesta/update", Richiesta.updateRichiesta);
app.delete("/api/richiesta/delete", Richiesta.eliminaRichiesta);

//Datausage
app.get("/api/datausage/all", Datausage.getAllDataUsage);
app.get("/api/datausage/get-by-id", Datausage.getDataUsageById);
app.get("/api/datausage/numOfShows", Datausage.getNumShows);
app.post("/api/datausage/save", Datausage.saveDataUsage);
app.put("/api/datausage/update", Datausage.updateDataUsage);
app.delete("/api/datausage/delete", Datausage.eliminaDataUsage);

//Cloud Object Storage
app.post("/api/cos/upload", checkJwt, Cos.upload);
app.get("/api/cos/get", checkJwt, Cos.getItem);
app.get("/api/sito/cos/get", Cos.getItem);
app.delete("/api/cos/delete", checkJwt, Cos.deleteItem);

//v1
app.get("/api/v1/get-tree", V1.getDataTreeView);
app.post("/api/v1/cloud-upload", V1.cloudUpload);
app.get("/api/v1/categoria/get-by-id", V1.getDataCategory);

//configuratore
app.get("/api/v1/configuratore-fwg", V1.configuratoreFWG);
app.get("/api/v1/configuratore-bwt", V1.configuratoreBWT);
app.get("/api/v1/configuratore-bws", V1.configuratoreBWS);
app.get("/api/v1/configuratore-hss", V1.configuratoreHSS);

//report
app.get("/api/v1/document-report", V1.getDocumentReportData);
app.get("/api/v1/document-report-detail", V1.getDocumentReportDataDetails);
app.get("/api/v1/user-report", V1.getUserReportData);
app.get("/api/v1/user-report-detail", V1.getUserReportDataDetail);
app.get("/api/v1/sys-report", V1.getDataUsageSys);
app.get("/api/v1/documents-summary", V1.getDocumentsData);

//mail
app.post("/api/sito/send-mail", Mail.sendEmail);

// Start the app
var port = process.env.PORT || 3001;
app.listen(port, () =>
  console.log("### Server in ascolto sulla porta: ", port, authConfig.domain)
);
