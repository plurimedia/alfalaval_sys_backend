module.exports = {
  devServer: {
    proxy: "http://localhost:3001"
  },

  pluginOptions: {
    quasar: {
      importStrategy: "kebab",
      rtlSupport: false
    }
  },

  transpileDependencies: ["quasar"],

  chainWebpack: config => {
    config.plugin("html").tap(args => {
      /* eslint-disable  no-param-reassign */
      args[0].title = "Size your system Backend • Alfa Laval";
      return args;
    });
  }
};
