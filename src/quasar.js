import Vue from "vue";

import "./styles/quasar.sass";
import lang from "quasar/lang/it.js";
import "@quasar/extras/material-icons/material-icons.css";
import { Quasar, Notify } from "quasar";

Vue.use(Quasar, {
  plugins: { Notify },
  config: { notify: {} },
  lang: lang
});
